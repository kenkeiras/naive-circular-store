from setuptools import setup

setup(name='naive-circular-store',
      version='0.1',
      description='Overly naive storage mechanism to read/write logs in a file with a circular-like structure.',
      author='kenkeiras',
      author_email='kenkeiras@codigoparallevar.com',
      license='Apache License 2.0',
      packages=['naive_circular_store'],
      scripts=['bin/naive-circular-store-api'],
      install_requires=[
          'tornado',
      ],
      zip_safe=True)
