import mmap
import os
import struct

ENDIANNESS_str = '!'

# Pointer to record
ptr_size = 8
ptr_str = 'Q'  # Unsigned long long
PTR_NO_RECORD = 0

# Counter
item_id_size = 8
item_id_str = 'Q'  # Unsigned long long

# Record size
size_size = 4
size_str = 'I'  # Unsigned int

M = 1 << 20
G = 1 << 30

MAGIC = b'RST1'

HEADER_SIZE = len(MAGIC) + ptr_size + ptr_size + item_id_size + item_id_size

class InvalidHeaderException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

def pack_size(size):
    return struct.pack(ENDIANNESS_str + size_str, size)

def pack_ptr(ptr):
    return struct.pack(ENDIANNESS_str + ptr_str, ptr)

def pack_item_id(item_id):
    return struct.pack(ENDIANNESS_str + item_id_str, item_id)

def unpack_size(raw):
    return struct.unpack(ENDIANNESS_str + size_str, raw)[0]

def unpack_ptr(raw):
    return struct.unpack(ENDIANNESS_str + ptr_str, raw)[0]

def unpack_item_id(raw):
    return struct.unpack(ENDIANNESS_str + item_id_str, raw)[0]


class CircularStore:
    def __init__(self, fpath, max_size=10*M):
        self.fpath = os.path.abspath(fpath)
        self.max_size = max_size
        self.f = None
        self.mm = None
        self._start_pointer = None
        self._end_pointer = None
        self._last_start = None
        self._first_item_id = None
        self._last_item_id = None
        self._initialize()

    def suspend(self):
        if self.f is not None:
            self.save()
            if self.mm is not None:
                self.mm.close()
            self.f.close()
        self.f = None
        self.mm = None
        self._start_pointer = None
        self._end_pointer = None
        self._last_start = None
        self._first_item_id = None
        self._last_item_id = None

    def save(self):
        handler = self.f
        if self.mm is not None:
            handler = self.mm

        handler.seek(0)
        handler.write(MAGIC
                      + pack_ptr(self._start_pointer)
                      + pack_ptr(self._last_start)
                      + pack_item_id(self._first_item_id)
                      + pack_item_id(self._last_item_id))

    def _initialize(self):
        if os.path.exists(self.fpath):
            self.f = open(self.fpath, 'r+b')
        else:
            self.f = open(self.fpath, 'w+b')

        # Check if the file header was already set
        data = self.f.read(HEADER_SIZE)
        if len(data) == 0:
            # No, so initialize it
            self._start_pointer = PTR_NO_RECORD
            self._end_pointer = PTR_NO_RECORD
            self._last_start = PTR_NO_RECORD
            self._first_item_id = 0
            self._last_item_id = 0
            self.save()
        elif len(data) != HEADER_SIZE:
            raise InvalidHeaderException('Header too small (expected {} bytes, found {})'.format(HEADER_SIZE, len(data)))
        elif data[:4] != MAGIC:
            raise InvalidHeaderException('Invalid magic number (expected {}, found {})'.format(MAGIC, data[:4]))
        else:
            (
                self._start_pointer, self._last_start, self._first_item_id, self._last_item_id
             ) = struct.unpack(ENDIANNESS_str + ptr_str + ptr_str + item_id_str + item_id_str, data[4:])
            self.f.seek(self._last_start + ptr_size)
            size_raw = self.f.read(size_size)
            size = unpack_size(size_raw)
            self._end_pointer = self._last_start + ptr_size + size_size + size

        self.f.truncate(self.max_size)
        self.mm = mmap.mmap(self.f.fileno(), self.max_size)
        self.mm.seek(0)

    def write(self, data):
        data_len = len(data)
        assert data_len > 0
        if self.mm is None:
            self._initialize()

        if data_len + HEADER_SIZE + 4 > self.max_size:
            raise Exception("Cannot store more than {} bytes".format(self.max_size - HEADER_SIZE - 4))

        if self.mm is None:
            self._initialize()

        if self._end_pointer == PTR_NO_RECORD:
            self.mm.seek(HEADER_SIZE)
            self._write_record(data, PTR_NO_RECORD)
            self._start_pointer = HEADER_SIZE
            self._last_start = HEADER_SIZE
            self._end_pointer = self.mm.tell()
            return

        if self._end_pointer + ptr_size + size_size >= self.max_size:
            # Wrap around
            self._end_pointer = HEADER_SIZE
        elif self._end_pointer + ptr_size + data_len + size_size >= self.max_size:
            self.mm.seek(self._end_pointer)
            self.mm.write(pack_ptr(PTR_NO_RECORD))
            self.mm.write(pack_size(0))
            self._end_pointer = HEADER_SIZE

        end_pos = self._end_pointer + data_len + size_size + ptr_size
        while (self._end_pointer <= self._start_pointer) and (end_pos >= self._start_pointer):
            # Clear one element
            if self._start_pointer + ptr_size + size_size >= self.max_size:
                self._start_pointer = HEADER_SIZE
            else:
                sz_raw = self.mm[self._start_pointer + ptr_size : self._start_pointer + ptr_size + size_size]
                rec_size = unpack_size(sz_raw)
                if rec_size == 0:
                    self._start_pointer = HEADER_SIZE
                else:
                    self._start_pointer += ptr_size + size_size + rec_size
                    self._first_item_id += 1

            self.mm[self._start_pointer: self._start_pointer + ptr_size] = pack_ptr(PTR_NO_RECORD)

        self.mm.seek(self._end_pointer)
        self._last_start = self._write_record(data, self._last_start)
        self._end_pointer = self.mm.tell()
        self._last_item_id += 1

    def _write_record(self, data, prev_record_pos):
        start = self.mm.tell()
        self.mm.write(pack_ptr(prev_record_pos))
        self.mm.write(pack_size(len(data)))
        self.mm.write(data)
        return start

    def get_size(self):
        return os.path.getsize(self.fpath)

    def get_last(self, n=1):
        if self.mm is None:
            self._initialize()

        pos = self._last_start
        for i in range(n):
            if pos < HEADER_SIZE:
                return

            assert pos < self.max_size
            previous_raw = self.mm[pos : pos + ptr_size]
            previous = unpack_ptr(previous_raw)

            size_raw = self.mm[pos + ptr_size: pos + ptr_size + size_size]
            size = unpack_size(size_raw)
            assert pos + size < self.max_size

            yield self.mm[ pos + ptr_size + size_size
                          : pos + ptr_size + size_size + size]
            pos = previous

    def get_all(self):
        if self.mm is None:
            self._initialize()

        if self._start_pointer == PTR_NO_RECORD:
            return []

        else:
            self.mm.seek(self._start_pointer)

        while True:
            pos = self.mm.tell()
            if pos == self._end_pointer:
                break

            if pos == self.max_size:
                self.mm.seek(HEADER_SIZE)
                continue

            self.mm.seek(pos + ptr_size)
            sz_raw = self.mm.read(size_size)
            if len(sz_raw) == 0:
                self.mm.seek(HEADER_SIZE)
                continue

            rec_size = unpack_size(sz_raw)
            if rec_size == 0:
                self.mm.seek(HEADER_SIZE)
                continue

            yield self.mm.read(rec_size)

    def get_all_batch(self):
        if self.mm is None:
            self._initialize()

        self.mm.seek(self._start_pointer + ptr_size)
        res = []

        while True:
            pos = self.mm.tell()
            if pos == self._end_pointer:
                break

            if pos == self.max_size:
                self.mm.seek(HEADER_SIZE)
                continue

            sz_raw = self.mm.read(size_size)
            if len(sz_raw) == 0:
                self.mm.seek(HEADER_SIZE)
                continue

            rec_size = unpack_size(sz_raw)
            if rec_size == 0:
                self.mm.seek(HEADER_SIZE)
                continue

            res.append(self.mm.read(rec_size))
            _ = self.mm.read(ptr_size)
        return res

    def get_item_by_id(self, item_id):
        if self.mm is None:
            self._initialize()

        assert self._first_item_id <= item_id <= self._last_item_id
        return self._get_item_by_id_from_start(item_id)

    def _get_item_by_id_from_start(self, item_id):

        pos = self._start_pointer
        curr_id = self._first_item_id

        while True:
            if pos == self._end_pointer:
                print(pos, curr_id, item_id)
                assert pos != self._end_pointer
            if pos == self.max_size:
                pos = HEADER_SIZE
                continue

            sz_raw = self.mm[pos + ptr_size : pos + ptr_size + size_size]
            if len(sz_raw) == 0:
                pos = HEADER_SIZE
                continue

            rec_size = unpack_size(sz_raw)
            if rec_size == 0:
                pos = HEADER_SIZE
                continue

            if curr_id == item_id:
                return self.mm[pos + ptr_size + size_size : pos + ptr_size + size_size + rec_size]

            curr_id += 1
            pos += ptr_size + size_size + rec_size
