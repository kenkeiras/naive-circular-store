#!/usr/bin/env escript
%% -*- erlang -*-
%%! -smp enable -sname factorial -mnesia debug verbose

-export([main/1]).
-define(API_HOST, "localhost").
-define(API_PORT, 5000).

-define(DB_NUM, 10).
-define(OPS_PER_DB, 100).

perform_single_write(Db, Data) ->
    Template = io_lib:fwrite("~10..0B", [Data]),
    Body = lists:flatten(lists:map(fun(_) -> Template end, lists:seq(1, 100))),
    Url = lists:flatten(io_lib:format("http://~s:~p/test-db-~p", [?API_HOST, ?API_PORT, Db])),
    Type = "application/octet-stream",
    Headers = [],
    HTTPOptions = [],
    Options = [],
    {ok, _} = httpc:request(post, {Url, Headers, Type, Body}, HTTPOptions, Options),
    ok.

perform_single_read(Db, Data) ->
    Url = lists:flatten(io_lib:format("http://~s:~p/test-db-~p?q=latest&n=~p", [?API_HOST, ?API_PORT, Db, 1])),
    Headers = [],
    HTTPOptions = [],
    Options = [{body_format, binary}],
    {ok, { {_, StatusCode, _StatusPhrase}, _Headers, Body }
    } = httpc:request(get, {Url, Headers}, HTTPOptions, Options),
    1000 = size(Body),
    2 = StatusCode div 100, %% Expect a 2XX status code.
    ok.

perform_reads() ->
    Orig = self(),
    ok = lists:foreach(fun(Db) ->
                               ok = lists:foreach(fun(Data) ->
                                                          spawn(fun() ->
                                                                        try perform_single_read(Db, Data) of
                                                                            X -> Orig ! X
                                                                        catch ErrorNS:Error:ST ->
                                                                                Orig ! {error, {ErrorNS, Error, ST}}
                                                                        end
                                                                end)
                                                  end, lists:seq(0, ?OPS_PER_DB - 1))
                       end, lists:seq(0, ?DB_NUM - 1)).

perform_writes() ->
    Orig = self(),
    ok = lists:foreach(fun(Db) ->
                          ok = lists:foreach(fun(Data) ->
                                                     spawn(fun() ->
                                                                   try perform_single_write(Db, Data) of
                                                                       X -> Orig ! X
                                                                   catch ErrorNS:Error:ST ->
                                                                           Orig ! {error, {ErrorNS, Error, ST}}
                                                                   end
                                                           end)
                                             end, lists:seq(0, ?OPS_PER_DB - 1))
                  end, lists:seq(0, ?DB_NUM - 1)).

seed_dbs() ->
    ok = lists:foreach(fun(Db) ->
                               ok = perform_single_write(Db, 99999)
                       end, lists:seq(0, ?DB_NUM - 1)).

wait_for(0) ->
    io:fwrite("~n");
wait_for(Remaining) ->
    receive
        ok ->
            io:fwrite("O"),
            wait_for(Remaining - 1);
        {error, Error} ->
            io:fwrite("~nError: ~p~n", [Error]),
            erlang:halt(error)
    end.

main(_) ->
    inets:start(),
    io:fwrite("Running API tests...~n"),
    seed_dbs(),
    perform_writes(),
    perform_reads(),
    wait_for((?DB_NUM * ?OPS_PER_DB) * 2),
    io:fwrite("OK~n").
