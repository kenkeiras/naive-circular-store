-- Send a test payload

counter = 1

filler_chunk = 'pack--TST-'
filler = ''
for i = 0,10,1 do
  filler = filler .. filler_chunk
end

request = function()
  wrk.method = "POST"
  wrk.body   = "" .. counter
  counter = counter + 1
  wrk.headers["Content-Type"] = "application/octet-stream"

  return wrk.format(nil, '/testdb')
end
