import math
import os
import struct
import time

from naive_circular_store import CircularStore

if os.path.exists('test.db'):
    os.unlink('test.db')
db = CircularStore('test.db')

print("Write test", end='', flush=True)
TO_ADD = 100*1024
ITEM_SIZE = 1024
t0 = time.time()
for i in range(TO_ADD):
    data = struct.pack('I', i) * (ITEM_SIZE >> 2)
    assert len(data) == ITEM_SIZE
    db.write(data)
    if i % 50000 == 0:
        print('.', end='', flush=True)

t1 = time.time()
tx = TO_ADD * ITEM_SIZE
print()
db.suspend()
print("  {:.1f}mb in {:.3f}s => {:.1f}mb/s".format(tx / (1 << 20), t1 - t0, (tx / (t1 - t0)) / (1 << 20)))
print()
assert(db.get_size() <= db.max_size)

print("Read earlier-to-latest test...")
count = 0
last = None
lower = None
t0 = time.time()
tx = 0
for rec in db.get_all():
    if len(rec) != ITEM_SIZE:
        print("Len:", len(rec))
    tx += len(rec)
    curr = struct.unpack('I', rec[:4])[0]
    assert len(rec) == ITEM_SIZE
    count += 1
    if last is not None:
        if curr <= last:
            print("a: {} > {}".format(curr, last))
        assert curr > last
    if lower is None:
        lower = curr
    last = curr
t1 = time.time()

discarded = TO_ADD - count
expected_discarded = math.ceil(max(0,
                                   (((4 + 8 + 8) + TO_ADD * (ITEM_SIZE + 8 + 4)))
                                   - db.max_size)
                               / ITEM_SIZE)

print("  added: {}; retrieved: {}; lower: {}; discarded: {}; expected ~{}".format(TO_ADD, count, lower, discarded, expected_discarded))
print("  {:.1f}mb in {:.3f}s => {:.1f}mb/s".format(tx / (1 << 20), t1 - t0, (tx / (t1 - t0)) / (1 << 20)))
print()

assert lower == discarded
assert last == TO_ADD - 1

# Id's are kept track of correctl
assert db._first_item_id == lower
assert db._last_item_id == last
db.suspend()

# Id's are saved and loaded correctly
db._initialize()
assert db._first_item_id == lower
assert db._last_item_id == last
db.suspend()

print("Batch  earlier-to-latest read test... ")
t0 = time.time()
tx = 0
for rec in db.get_all():
    tx += len(rec)
    assert len(rec) == ITEM_SIZE
t1 = time.time()

print("  {:.1f}mb in {:.3f}s => {:.1f}mb/s".format(tx / (1 << 20), t1 - t0, (tx / (t1 - t0)) / (1 << 20)))
print()
db.suspend()

recoverable = 0
last = None
print("Read latest-to-earlier... ")
tx = 0
t0 = time.time()
for rec in db.get_last(n=TO_ADD * 2):
    tx += len(rec)
    curr = struct.unpack('I', rec[:4])[0]
    if last is not None:  # Retrieved latest-to-oldest
        if not (curr < last):
            print("assert({} < {})".format(curr, last))
        assert curr < last
    last = curr
    recoverable += 1
t1 = time.time()
assert recoverable == count
print("  {:.1f}mb in {:.3f}s => {:.1f}mb/s".format(tx / (1 << 20), t1 - t0, (tx / (t1 - t0)) / (1 << 20)))


print("\nChecking latest-to-earlier read non-linearities...")
max_power_10 = math.floor(math.log(count, 10))
for power10 in range(0, max_power_10 + 1):
    t0 = time.time()
    latest = list(db.get_last(n=10 ** power10))
    t1 = time.time()
    db.suspend()
    throughput = (len(latest) * ITEM_SIZE) / (t1 - t0)

    print(("  Read {:" + str(max_power_10 + 1) + "} last in {:.4f}s = {:5.1f}mb/s").format(10 ** power10, t1 - t0, throughput / (1 << 20)))
    assert len(latest) == 10 ** power10


print("\nChecking get-by-id...")
db._initialize()
mid = math.ceil((db._first_item_id + db._last_item_id) /  2)
checks = (
    ('first', db._first_item_id),
    ('10 after the first', db._first_item_id + 10),
    ('last', db._last_item_id),
    ('10 before the end', db._last_item_id - 10),
    ('middle', mid),
    ('middle - 1', mid - 1),
    ('middle - 2', mid - 2),
    ('middle + 1', mid + 1),
)
db.suspend()

for expl, i in checks:
    t0 = time.time()
    rec = db.get_item_by_id(i)
    t1 = time.time()
    db.suspend()

    val = struct.unpack('I', rec[:4])[0]

    print(("  [{:.5f}s] Read {}").format(t1 - t0, expl))
    assert i == val
