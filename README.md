# NaiveCircularStore

## Disclaimer

**This code IS NOT THROUGHOUTLY TESTED, and it is probable that race conditions or straight errors CAN CORRUPT THE DATA IT STORES.**

**Consider other alternatives and perform your own assessment of this code before deploying it.**

**Specifically: At this point this library does not do automatically `fsync(2)`, so it's very likely to make the data corrupted.**

## Purpose

This is a naive storage mechanism to read/write logs in a file with a circular-like structure.

It's goal is to allow to allocate a specific amount of storage to a given file and then write to it continuously.
When more data overflows the space allocated for it, older records are overwritten (unless data becomes corrupted, records are either present or considered free space, no record should be corrupted).

### Capabilities and (approximated) complexity
 - **Write**: `O(1)`, around the same performance as appending to a raw file.
 - **Read N records from latest**: `O(number of records read)`
 - **Read N records from older**: `O(number of records read)`
 - **Read record by id**: `O(number of records present in the full DB)`, it's *slightly* faster to reading records from the older to the record ID.

### Not capable of
 - Deleting specific records: older records may be overwritten by later ones, but specific deletions (or setting flags) is not supported.
 - Authenticating: No user/password authentication is made, a firewall or gateway must be used to set this control.

## Usage

### API

 - Launch with Docker

   ```shell
   # Build docker image
   docker build -t naive-circular-store .
   docker run -d --name circular-store \
         -e STORAGE_PATH='/storage' -v `pwd`/_storage:/storage \
         -p 127.0.0.1:5000:5000 \
         naive-circular-store
   ```

 - Remove docker

   ```shell
   docker stop circular-store
   docker rm circular-store
   # Note that the files are kept on the `_storage` directory
   ```

#### API endpoints

 - `POST /<database name>`: Write a record to the given database (it's created if it does not exist)
 - `GET /<database name>?q=latest&n=<number>`: Retrieve `number` elements from the database (separated by \0 bytes) starting with the latest. If no `q` is passed to the endpoint this is the default one. If no `n` is passed, `1` will be taken as default.
 - `GET /<database name>?q=first&n=<number>`: Retrieve `number` elements from the database (separated by \0 bytes) starting with the oldest. If no `n` is passed, `1` will be taken as default.
 - `GET /<database name>?q=all`: Get all records from the database (separated by \0 bytes).

## Scripts

 - `sh scripts/test-api.sh`: Use [wrk](https://github.com/wg/wrk/) to run some tests on the server (expects one on `127.0.0.1:5000`).
 - `python3 scripts/test.py`: Runs some simple tests on the package as python library (not through the API).
 - `escript scripts/test_api.erl`: Run a simple concurrency test on the server.
